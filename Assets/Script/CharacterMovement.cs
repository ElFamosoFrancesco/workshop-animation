using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    public float speed;

    public float rotationSpeed;

    public Vector2 direction;

    public Vector2 directionBase = new Vector2(0f, 0f);

    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        // Make it move 10 meters per second instead of 10 meters per frame...
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;

        direction = new Vector2(Input.GetAxis("Horizontal") * speed, Input.GetAxis("Vertical") * speed);
        direction *= Time.deltaTime;

        // Move translation along the object's z-axis
        // transform.Translate(0, 0, translation);
        transform.Translate(direction.x, 0, direction.y, Space.World);

        // Rotate around our y-axis
        // transform.Rotate(0, Vector2.Angle(direction, directionBase), 0);

        if (direction.magnitude > 0f)
        {
        transform.forward = new Vector3(-direction.x, 0, -direction.y);
            anim.SetBool("Run", true);
        }
        else
        {
            anim.SetBool("Run", false);
        }

        if (Input.GetButtonDown("Punch"))
        {
            anim.SetTrigger("Punch");
        }

        if (Input.GetButtonDown("Kick"))
        {
            anim.SetTrigger("Kick");
        }

        if (Input.GetButtonDown("Jump"))
        {
            anim.SetTrigger("Jump");
        }

        if (Input.GetButtonDown("Special"))
        {
            anim.SetTrigger("Special");
        }
    }
}
